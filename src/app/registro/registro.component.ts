import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  public usuarioRegistro: Usuario = new Usuario(null,null);
  public listUsuarios: Array<Usuario> = [];

  constructor() { }

  ngOnInit() {
  }
  registroUsuario(usuario:Usuario){
    this.listaUsuarios.push(usuario);
  }
}
